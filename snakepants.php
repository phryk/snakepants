<?php

/**
 *      snakepants website stuff
 *      phpdoc is shit.
 *
 *
 *                                 ._________________.
 *                                /                   \
 *                               .      tiny snake     .
 *                               \  thinks PHP sucks! /
 *                                \._________________.
 *                                /
 *                               °
 *                          ___
 *                         / T \_¸
 *           ___          / __ / `  
 *       ___/   \________/  / 
 *      <_____/____________/
 *
 *
 *
 *                          .-=     Core concepts    =-.
 *
 *      Router:
 *
 *          The router associates route paths (like "foo/<str:arg_one>/<int:arg_two>/")
 *          to a Handler class, optionally with an ordering for the 
 *          parameters. The router object is created here in index.php
 *          and instantiates the Handler class associated with the current
 *          requests route, passing the correct parameters extracted from
 *          the url using the route path.
 *
 *          Form more details, refer to the documentation of `routing.php`.
 *
 *
 *      Renderable:
 *
 *          A `Renderable` is any instance of any class implementing the
 *          `Renderable` interface. This makes sure it implements a public
 *          `render` function which is supposed to return an HTML
 *          representation of the given object.
 *
 *          For more details, refer to the documentation of `renderables.php`.
 *
 *
 *      Handler:
 *
 *          A `Handler` is any instance of any class implementing the `Handler`
 *          interface. This makes sure it implements a public `handle` function.
 *
 *          When a route path regexp known to the router is matched by the
 *          current value of $_GET['p'], it tries to instantiate the associated
 *          Handler class and prints the return value of its `handle` function.
 *
 *          For more details, refer to the documentation of `handlers.php`.
 *
 *
 *      Storage:
 *
 *          General purpose storage facility.
 *          This class does *all* communication with the database.
 *
 *          For more details, refer to the documentation of `storage.php`.
 *
 *
 *      Storable:
 *
 *          A storable is any instance of any class implementind the
 *          `Storable` interface.
 *
 *          All storables have $id and $name properties. This is taken
 *          care of by the `Storage` facility.
 *
 *          For more details, refer to the documentation of `storage.php`.
 *
 */

if(!session_start()){
    throw new Exception("Couldn't get session.");
}


if(!array_key_exists('flashes', $_SESSION)){
    $_SESSION['flashes'] = array();
}

// $_GET['p'](ath) defaults to ''
if(!array_key_exists('p', $_GET)){
    $_GET['p'] = '';
}

set_exception_handler('handle_exception');


function flash($message, $type = 'notification'){
    $_SESSION['flashes'][] = array(
        'message' =>$message,
        'type' => $type,
    );
}


function get_config(){
    global $config;
    return $config;
}


function get_router(){
    global $router;
    return $router;
}


function get_storage(){
    global $storage;
    return $storage;
}


function get_directory(){

    $segments = explode('/', $_SERVER['PHP_SELF']);
    return substr($_SERVER['SCRIPT_FILENAME'], 0, (strlen(array_reverse($segments)[0]) * -1));
}


function get_implementing_classes($interface){

    /*
     * Gets all classes implementing $interface, except for the Base implementation.
     *
     * The base implementation, by convention, is named Base<$interface>. (i.e. 'BaseExample')
     */

    $implementing = array();
    $base = sprintf("Base%s", $interface);

    foreach(get_declared_classes() as $class){

        // Filter for any class that implements $interface, except for the Base implementation
        if($class != $base && array_key_exists('Storable', class_implements($class))){
            $implementing[strtolower($class)] = $class;
        }
    }

    return $implementing;
    
}


function get_user(){

    if(array_key_exists('uid', $_SESSION) && ctype_digit((string)$_SESSION['uid'])){
        $user = new User($_SESSION['uid']);
        if($user){ // TODO: !== FALSE, if necessary
            return $user;
        }
    }

    return FALSE;

}


function build_url($path, $protocol_override=FALSE){
    
    global $config;

    if($protocol_override){
        $protocol = $protocol_override;
    } elseif(
        (!array_key_exists('uid', $_SESSION) && $config['force_ssl_anon']) ||
        (array_key_exists('uid', $_SESSION) && $config['force_ssl_user'])
    ){
        $protocol = 'https';
    } else {
        $protocol = 'http';
    }

    return sprintf("%s://%s%s", $protocol, $config['base_url'], $path);
}


function redirect($path=''){
    header(sprintf("Location: %s", build_url($path)));
}


function handle_exception($e){

    //TODO: Whitelist of Exception types to show messages to end user; Replace message for all other Exception types

    global $config;

    if(!$config['debug']){
        ob_end_clean(); // If we don't do this, partial render_template() will show up, even without explicitly printing them!
    }

    try {
        if($config['debug']){
            $msg = $e->getMessage();
            $info = var_export(debug_backtrace(), TRUE);
        } else {
            $msg = 'Oh noes. Something went wrong!';
            $info = FALSE;
        }

        $ep = new ErrorPage($msg, 500, $info);
        print $ep->handle();
    } catch(Exception $e){

        if($config['debug']){
            print $e->getMessage();
        } else {
            print file_get_contents(get_directory().'snakepants/static/fail.html');
        }
    }

}


require_once('defaults.php'); // Initialize config arrays with defaults
require_once('routing.php');
require_once('renderables.php');
require_once('handlers.php');
require_once('storage.php');
require_once('auth.php');
