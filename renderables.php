<?php

require_once('themes/util.php');

class RenderException extends Exception{}


function find_template($templates){

        global $config;

        if(!is_array($templates)){
            $templates = array($templates);
        }

        $base_dir = get_directory();
        $theme = array_key_exists('theme', $config) ? $config['theme'] : 'default';
        
        $dirs = array();
        if($theme != 'default'){
            $dirs[] = sprintf("%sthemes/%s/", $base_dir, $theme);
            $dirs[] = sprintf("%ssnakepants/themes/%s/", $base_dir, $theme);
        }

        $dirs[] = sprintf("%ssnakepants/themes/default/", $base_dir);

        $paths = array();

        foreach($dirs as $dir){
            foreach($templates as $template){
                $paths[] = $dir.$template.'.tpl.php';
            }
        }

        foreach($paths as $path){

            if(file_exists($path)){
                return $path;
            }
        }

        throw new RenderException(sprintf("Can't find any of these templates: %s", implode(",\n ", $paths)));
    }

function render_template($templates, $vars){
    if(!is_array($templates)){
        $templates = array($templates);
    }

    $tpl = find_template($templates);

    extract($vars);
    ob_start();
    $load_success = include($tpl);
    $template_rendered = ob_get_contents();
    ob_end_clean();

    if($load_success){
        return $template_rendered;
    } else {
        throw new RenderException(sprintf("Could not render template: %s.", $tpl));
    }
}


interface Renderable {
    
    public function render($mode = 'full');

}


class BaseRenderable implements Renderable {


    public function __toString(){
        return $this->render('teaser');
    }


    public function render($mode = 'full'){

        $base_name = strtolower(get_class($this));
        $tpls = array(
            $base_name."-".$mode,
            $base_name,
        );

        return render_template($tpls, array('obj' => $this, 'mode' => $mode));
    }
}
