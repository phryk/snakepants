<?php

require_once('snakepants/snakepants.php');
require_once('config.php');
require_once('content.php');

$router = new Router();
$storage = new Storage();

// Add routes
$router->add_route('', 'Frontpage');
$router->add_route('produkt/<str:id_or_name>', 'Produkt'); // Produkt defined in content.php
$router->add_route('test', 'Test');

print $router->handle($_GET['p']);
