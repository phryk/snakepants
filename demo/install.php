<?php

require_once('snakepants/snakepants.php');
require_once('config.php');
require_once('content.php'); // Load the file which holds our storables

$storage = new Storage();

foreach($storage->get_storables() as $storable){
    $storage->exec($storable, 'create_table');
}
