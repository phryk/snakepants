<?php
    
require_once('renderables.php');


interface Handler extends Renderable {

    public function url($mode='view');
    public function handle();

}



class BaseHandler extends BaseRenderable implements Handler {


    public function url($mode='view'){

        $router = get_router();

        return $router->url($this, $mode);
    }


    public function handle($mode='full'){
        if($_SERVER['REQUEST_METHOD'] == 'GET'){

            $sections = array(
                'header' => array(),
                'content' => array($this),
                'aside' => array(),
                'footer' => array(),
            );

            $vars = array(
                'flashes' => $_SESSION['flashes'],
                'sections' => $sections,
                'content_mode' => $mode,
            );

            $output = render_template('main', $vars);
            $_SESSION['flashes'] = array(); // Should only be executed, if previous render_template call is successful.
            return $output;

        } else {
            throw new Exception(sprintf('HTTP Method not supported: %s', $_SERVER['REQUEST_METHOD']));
        }
    }
}



class ErrorPage extends BaseHandler {

    public $msg;
    public $code;
    public $info;

    function __construct($msg, $code=500, $info=FALSE){

        $this->msg = $msg;
        $this->code = $code;

        if($info){
            $config = get_config();
            if(!$config['debug'] === TRUE){
                $info = 'Debugging information withheld.';
            }
        }

        $this->info = $info;
    }


    function render($mode='full'){
        http_response_code($this->code);
        return parent::render($mode);
    }
}



class Frontpage extends BaseHandler {} // Basic Frontpage is just its template content.
