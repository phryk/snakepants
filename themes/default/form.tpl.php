<form method="POST" enctype="multipart/form-data">
    <?php foreach($obj->get_field_instances() as $field): ?>
        <?php print $field->render(); ?>
    <?php endforeach; ?>

    <div class="controls">
        <button type="submit">Submit</button>
    </div>

</form>
