<div class="listing <?php print $obj->class; ?> mode-<?php print $obj->mode; ?>">
    <?php foreach($obj->items as $item): ?>
    <?php print $item->render($obj->mode); ?>
    <?php endforeach; ?>
</div>
