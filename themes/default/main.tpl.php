<!doctype html>
<html>

    <body>
        <header>
            <h1>Header shiet</h1>
            <?php if($user = get_user()): ?>
            <h2>Logged in as <em><?php print $user->name; ?></em></h2>
            <?php endif; ?>
        </header>

        <?php if(count($flashes)): ?>
        <section id="flashes">
            <ul>
            <?php foreach($flashes as $flash): ?>
                <li class="<?php print $flash['type']; ?>">
                    <?php print $flash['message']; ?>
                </li>
            <?php endforeach; ?>
            </ul>
        </section>
        <?php endif; ?>

        <main>
        <?php foreach($sections['content'] as $content): ?>
            <?php print $content->render($content_mode); ?>
            <?php endforeach; ?>
        </main>

        <footer>
            <pre>
                                          ._________________.
                                         /                   \
                                        .      tiny snake     .
                                        \  thinks PHP sucks! /
                                         \._________________.
                                         /
                                        °
                                   ___
                                  / T \_¸
                    ___          / __ / `  
                ___/   \________/  / 
               <_____/____________/
            </pre>
        </footer>

    <body>
</html>
