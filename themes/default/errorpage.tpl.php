<div class="error">
    <h4 class="error-code"><?php print $obj->code; ?></h4>
    <pre><?php print htmlentities($obj->msg); ?></pre>
    <?php if($obj->info): ?>
        <div class="error-info">
            <pre><code><?php print $obj->info; ?></code></pre>
        </div>
    <?php endif; ?>
</div>
