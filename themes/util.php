<?php

/**
 * theming utility functions
 *
 */

 function tabs($obj){

    $modes = array('full', 'edit', 'delete');
    $tabs = array();

    foreach($modes as $mode){
        if($url = $obj->url($mode)){
            $tabs[] = '<li class="tab"><a href="'.$url.'">'.$mode.'</a></li>';
        }
    }

    $html = implode('', $tabs);
    return sprintf('<ul class="tabs">%s</ul>', $html);
 }
