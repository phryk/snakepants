<?php

require_once('renderables.php');

// TODO: Special values to document: Route::$class == '!dynamic' w/ dynamic path parameter 'class', Route::$static_params['mode']
// Other special routing parameters: class, id_or_name

class RoutingException extends Exception {}

class Router {

    /**
     * Collects routes, handles page requests accordingly
     *
     * Router paths:
     *      Router paths are made up of zero or more path segments.
     *
     *      Every path segment can be either a static string or a named variable.
     *
     *      named variables are noted in the form of <type:variable name>.
     *      type can be one of the following:
     *          * int
     *          * str
     *          * storable - name of any class implementing the Storable interface
     */

    public $routes;


    function __construct(){

        $this->routes = array();
        $this->add_route('register', 'Registration');
        $this->add_route('login', 'Login');
        $this->add_route('logout', 'Logout');
        $this->add_route('admin', 'Frontpage');

        $this->add_route(
            'admin/<storable:class>?<int:offset>/<int:pagination>',
            'Listing',
            array('mode' => 'admin-teaser')
        );

        $this->add_route(
            'admin/<storable:class>/<str:id_or_name>',
            '!dynamic',
            array('mode' => 'edit')
        );
    }


    public function add_route($pattern, $class, $static_params=array()){

        if(!array_key_exists($pattern, $this->routes)){

            $this->routes[$pattern] = new Route($pattern, $class, $static_params);

        } else {
            throw new Exception(sprintf("Tried to add route '%s' multiple times.", $pattern));
        }
    }


    public function handle($path){

        if(substr($path, -1, 1) === '/'){
            $path = substr($path, 0, (strlen($path) - 1));
        }


        // Not horifically efficient, but we're only dealing with a handful of routes
        foreach($this->routes as $route){

            if(is_array($params = $route->match($path))){ // TODO: Change match() to throw Exception and go with try...except?

                // Collect static parameters
                foreach($route->static_params as $param){
                    if($param['type'] === 'static'){
                        $params[$param['name']] = $param['value'];
                    }
                }

                // Determine the (proposed, in case of !dynamic) Handler class
                if($route->class == '!dynamic'){
                    $class = $params['class'];
                } else {
                    $class = $route->class;
                }

                // Load a reflection of the Handler class for introspection and instantiation
                $reflection = new ReflectionClass($class);
                
                $is_storable = array_key_exists('Storable', class_implements($class));

                // Load a reflection of the Handler classes constructor, or its static load function (if it's a Storable)
                if($is_storable){
                    $method = $reflection->getMethod('load');
                } else {
                    $method = $reflection->getConstructor();
                }

                $instantiation_params = array();

                if(get_class($method) === 'ReflectionMethod'){
                    $param_infos = $method->getParameters();
                    foreach($param_infos as $param_info){
                        $param_name = $param_info->getName();
                        if(array_key_exists($param_name, $params)){
                            $instantiation_params[] = $params[$param_name];
                        } elseif(!$param_info->isOptional()){
                            $fmt = "Non-optional parameter '%s' missing for Handler class '%s'.";
                            throw new RoutingException(sprintf($fmt, $param_name, $class));
                        } else {
                            // Set default value for non-passed optional parameter in case a later optional parameter *is* passed.
                            $instantiation_params[] = $param_info->getDefaultValue();
                        }
                    }
                }

                $instance = $is_storable
                    ? $method->invokeArgs(NULL, $instantiation_params)
                    : $reflection->newInstanceArgs($instantiation_params);

                return $instance->handle($route->mode);

            }
        }

        $page = new ErrorPage("This doesn't seem to exist, sorry.", 404);
        return $page->handle();
    }

    public function url($obj, $mode){

        /**
         * Get the correct link (if any) for the given object in this Router.
         */

        $class = get_class($obj);

        foreach($this->routes as $route){

            if(in_array($route->class, array($class, '!dynamic')) && $route->mode == $mode){

                $url_params = array();
                foreach($route->dynamic_params as $pname){

                    if(substr($pname, 0, 1) === '!'){
                        // Do nothing, used for parameters like !static_%d
                    } elseif(property_exists($obj, $pname)){
                        $url_params[$pname] = $obj->$pname;
                    } elseif($pname === 'class'){
                        $url_params[$pname] = strtolower(get_class($obj));
                    } elseif($pname === 'id_or_name') {
                        $url_params[$pname] = $obj->name;
                    } else {
                        $fmt = "Could not create URL for Route '%s' from given object. Missing parameter '%s'.";
                        throw new RoutingException(sprintf($fmt, $route->pattern, $pname));
                    }

                }

                return $route->url($url_params);
            }
        }
    }
}



class RoutePath {

    public $typepatterns = array(
        'int' => '\d+',
        'str' => '[\w-äöüÄÖÜß]+',
        'storable' => '\w+',
    );

    public $pattern;
    public $param_groups;
    public $param_list; // Flat array with ordered names of all parameters

    public function __construct($pattern){

        $this->pattern = $pattern;
        $this->param_groups = $this->parse_pattern($this->pattern);

        $this->param_list = array();
        foreach($this->param_groups as $param_group){
            foreach($param_group as $param){
                $this->param_list[] = $param['name'];
            }
        }
    }


    public function match($path){

        $group_offset = 0;
        $segment_offset = 0;
        $segments = explode('/', $path);
        $values = array(); // holds values of parameters as supplied by the path, keyed by parameter name. returned if path is successfully matched.

        foreach($this->param_groups as $param_group){

            $num_segments = count($param_group);
            $group_segments = array_slice($segments, $group_offset, $num_segments, TRUE);

            if(count($group_segments) != $num_segments){ // (rest of) the path doesn't have enough segments to match this parameter group.
                if($group_offset > 0 ){
                    break; // Only the first parameter group is obligatory
                } else {
                    return FALSE;
                }
            }

            foreach($param_group as $param){

                $segment = $segments[$group_offset + $segment_offset];

                $value = $this->match_param($param, $segment);
                if($value === FALSE){
                    return FALSE;
                }

                if($param['type'] != 'fixed'){
                    $values[$param['name']] = $value;
                }
                
                $segment_offset++;
            }

            $segment_offset = 0;

            $group_offset += $num_segments;
        }


        if(count($segments) !== $group_offset){
            return FALSE;
        }
        return $values;
    }


    public function url($param_vals){

        $segments = array();

        foreach($this->param_groups as $params){

            foreach($params as $key => $param){

                if($param['type'] == 'fixed'){

                    $segments[] = $param['pattern'];

                } elseif(array_key_exists($key, $param_vals)){

                    $segments[] = $param_vals[$key];

                }  else {

                    $msg = sprintf(
                        "Link could not be generated. Parameter '%s' is not defined
                        as fixed segment and has not been set. Set parmeters were: %s",
                        $key,
                        implode(',', array_keys($param_vals))
                    );

                    throw new Exception($msg);
                }
            }
        }

        return build_url(implode('/', $segments));
    }


    protected function parse_pattern($pattern){

        $param_groups = array();
        $pattern_groups = explode('?', $pattern);

        foreach($pattern_groups as $pattern_group){
            $param_groups[] = $this->parse_group($pattern_group);
        }

        return $param_groups; // Does this make sense?

    }


    protected function parse_group($pattern){

        $params = array();
        $fc = 0; // fixed param count, used for generating array keys of $params

        $segments = explode('/', $pattern);
        foreach($segments as $segment){

            $param = array();

            $matches = array();
            if(preg_match_all('|^<(?P<type>\w+):(?P<name>\w+)>$|', $segment, $matches)){

                $key = $matches['name'][0];

                $param['type'] = $matches['type'][0];
                $param['name'] = $matches['name'][0];
                $param['pattern'] = $this->typepatterns[$param['type']];
            
            } else {
                $fc++;

                $param['type'] = 'fixed';
                $param['name'] = sprintf("!fixed_%d", $fc); // '!' to avoid name collisions, if anyone chooses to name a parameter fixed_<n>.
                $param['pattern'] = $segment;

            }

            $params[$param['name']] = $param;
        }

        return $params;
    }
    
    protected function build_param_regex($param){

        $pattern_format = '(?P<%s>%s)'; // named group, first parameter is group name, second is expression to match
        $pattern_segments = sprintf($pattern_format, $param['name'], $param['pattern']);

        return sprintf("|^%s/{0,1}$|", $pattern_segments); // The whole string must match the named group, plus an optional forward slash (which is probably not needed and should be removed after this is working)
    } 


    protected function match_param($param, $segment){

        if($param['type'] === 'fixed'){

            if($segment !== $param['pattern']){ // fixed parameter doesn't match.
                return FALSE;
            }

            return $param['pattern'];

        } else {

            $regex = $this->build_param_regex($param);
            $matches = array();
            if(!preg_match($regex, $segment, $matches)){ // Presented segment does not match this parameter.
                return FALSE;
            }

            if($param['type'] == 'storable'){ // Make sure string given in $segment is actually the name of a Storable class.
                $storage = get_storage();
                if(!in_array($segment, array_keys($storage->get_storables()))){
                    flash('Storable segment not checking out: '.$segment);
                    return FALSE;
                }
            }

            return $matches[$param['name']];
        }
    }
}



class Route {

    public $pattern;
    public $class;
    public $mode;
    public $dynamic_params;
    public $static_params;

    protected $routepath;

    function __construct($pattern, $class, $static_params=array()){

        $this->routepath = new RoutePath($pattern);

        $this->pattern = $pattern;
        $this->class = $class;
        $this->mode = array_key_exists('mode', $static_params) ? $static_params['mode'] : 'full';
        $this->dynamic_params = $this->routepath->param_list;

        $this->static_params = array();
        foreach($static_params as $k => $v){

            if(in_array($k, $this->dynamic_params)){
                $fmt = "Parameter '%s' in Route '%s' supplied both as dynamic and static parameter, choose one.";
                throw new RoutingException(sprintf($fmt, $k, $this->pattern));
            }

            $this->static_params[$k] = array(
                'name' => $k,
                'value' => $v,
                'type' => 'static',
            );
        }

    }


    public function match($path){
        return $this->routepath->match($path);
    }


    public function url($param_vals){
        return $this->routepath->url($param_vals);
    }
}
