<?php

/**
 * This file defines functionality needed for persistant data. I.e. "Database stuff"
 *
 */

require_once('renderables.php');
require_once('handlers.php'); 
require_once('config.php');

class StorageException extends Exception{}
class ValidationError extends StorageException{}


interface Field {

    public function __construct($name);
    public function value();
    public function fill($data, $obj);
    public function save(); // For saving data apart from the 'main' table of a Storable class.
}



interface Storable {

    public static function load($id_or_name);
    public static function listing($limit, $offset);
    public function form();
    public function save();
}



class Storage {

    /**
     * Storage. Does all the communication with the database.
     *
     * Builds prepared statements, etc.
     */

    protected $connection;
    protected $storables;
    protected $statements; // holds all automatically prepared statements, keyed by class and action
    protected $parameters; // holds the names of parameters for automatically prepared statements, keyed by class and action


    public function __construct($config_override=FALSE){

        global $config;

        $options = array(); // This array will hold the data actually used to connect to the database.
        $config_keys = array('db_host', 'db_name', 'db_user', 'db_pw');
        $fmt_config_missing = "No %s given to Storage, none defined in \$config either.";

        if(is_array($config_override)){
            foreach($config_override as $key => $value){
                $options[$key] = $value;
            }
        }

        foreach($config_keys as $key){
            if(!array_key_exists($key, $options)){ // Get option from config, if it's not supplied in $config_override.
                if(array_key_exists($key, $config)){
                    $options[$key] = $config[$key];
                } elseif($config['debug']) {
                    throw new StorageException(sprintf($fmt_config_missing, $key)); // Throw an exception if an option is supplied neither in $config nor $config_override
                }
            }
        }

        $this->storables = array();
        $this->statements = array();
        $this->parameters = array();

        $db_url = sprintf("mysql:host=%s;dbname=%s", $options['db_host'], $options['db_name']);
        $this->connection = new PDO($db_url, $options['db_user'], $options['db_pw']);

        $storables = $this->get_storables();
        foreach($storables as $storable){
            $reflection = new ReflectionClass($storable);
            $dummy = $reflection->newInstance();
            $this->prepare_statements($storable, $dummy->get_field_instances());
        }
    }

    
    public function get_storables(){ // NOTE: At some point a $force_update parameter might make sense

        if(!count($this->storables)){

            foreach(get_declared_classes() as $class){

                // Filter for any class that implements the `Storable` interface, except for BaseStorable.
                if($class != 'BaseStorable' && array_key_exists('Storable', class_implements($class))){
                    $this->storables[strtolower($class)] = $class;
                }
            }
        }

        return $this->storables;
    }


    public function get_class($storable){
        return new ReflectionClass($storable);
    }


    public function prepare_statements($class, $fields){

        if(!array_key_exists($class, $this->statements)){

            $table = strtolower($class);
            $names = array_merge(array('name'), array_keys($fields));
            $placeholders = array();

            foreach($names as $name){
                $placeholders[] = ':'.$name;
            }


            // Potential exercise: optimize query building
            $this->statements[$class] = array();

            //CREATE TABLE
            $columns = array();
            foreach($fields as $name => $field){
                $column = $name.' '.$field->type;

                if($field->not_null){
                    $column .= ' NOT NULL';
                }

                $columns[] = $column;
            }

            $format = "CREATE TABLE IF NOT EXISTS %s (id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, name VARCHAR(100) UNIQUE, %s)";
            $query = sprintf($format, $table, implode(', ', $columns));
            $this->add_statement($class, 'create_table', $query, array());


            // SELECT (listing)
            $format = "SELECT id FROM %s ORDER BY id ASC LIMIT :limit OFFSET :offset";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'list', $query, array('limit', 'offset'));

            // SELECT (listing, descending)
            $format = "SELECT id FROM %s ORDER BY id DESC LIMIT :limit OFFSET :offset";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'list_desc', $query, array('limit', 'offset'));

            // SELECT (by id)
            $format = "SELECT * FROM %s WHERE id=:id";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'load', $query, array('id'));

            // SELECT (by name)
            $format = "SELECT * FROM %s WHERE name=:name";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'load_by_name', $query, array('name'));

            // INSERT
            $format = "INSERT INTO %s (%s) VALUES (%s)";
            $query = sprintf($format, $table, implode(',', $names), implode(',', $placeholders));
            $this->add_statement($class, 'add', $query, $names);

            // UPDATE
            $columns = array();
            foreach($names as $name){
                $columns[] = $name."=:".$name;
            }

            $format = "UPDATE %s SET ";
            $format .= implode(', ', $columns);
            $format .= " WHERE id=:id";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'update', $query, $names);

            // DELETE
            $format = "DELETE FROM %s WHERE id=:id";
            $query = sprintf($format, $table);
            $this->add_statement($class, 'delete', $query, array('id'));

            $reflection = new ReflectionClass($class);
            if($reflection->hasMethod('prepare_statements')){

                $method = $reflection->getMethod('prepare_statements');
                if($method::IS_STATIC){
                    $extra_statements = $method->invoke(NULL);

                    foreach($extra_statements as $op => $info){
                        $this->add_statement($class, $op, $info['query'], $info['parameters']);
                    }
                }
            }
        }
    }


    public function add_statement($cls, $op, $query, $parameters){
        $this->statements[$cls][$op] = $this->connection->prepare($query);
        $this->parameters[$cls][$op] = $parameters;
    }


    public function exec($class_or_obj, $action, $param_override=NULL){

        /**
         * Execute a query prepared with prepare_statements.
         *
         * @param mixed $class_or_obj Depending on $action, this parameter expects different types of values.
         *
         *          If this is an object implementing the Storable interfaces, it is used as a Storable, otherwise as a Storable class name.
         *
         *          If $action is 'create_table', 'load', or 'load_by_name' this is supposed to be the name of a
         *          Storable class for which the action is executed.
         *
         *          If $action is 'add', 'update' or 'delete' this is supposed to be the storable object
         *          the action is applied to.
         *
         *          Custom actions' expectations should be documented in the Storables' prepare_statements method.
         *
         * @param string $action The action to execute. This can be any action prepared for in prepare_statements().
         *          Namely, these are: create_table, load, load_by_name, add, update and delete.
         *
         * @param mixed $param_override Optional. This is supposed to be an id (int) for $action 'load' and string for 'load_by_name'.
         *          Otherwise, this is supposed to be a string-indexed array, used to override the parameters passed with the query.
         *
         */
        
        global $config;

        if(is_object($class_or_obj) && in_array('Storable', class_implements(get_class($class_or_obj)))){
            $obj = $class_or_obj;
            $class = get_class($obj);
        } else {
            $obj = FALSE;
            $class = $class_or_obj;
        }

        $statement = $this->statements[$class][$action];
        $param_names = $this->parameters[$class][$action];

        $params = array();

        if($obj !== FALSE){
            // Extract params from passed object.
            foreach($param_names as $name){
                $params[':'.$name] = $obj->$name;
            }

        } elseif($action === 'load'){
            $params[':id'] = $param_override;

        } elseif($action === 'load_by_name'){
            $params[':name'] = $param_override;

        } elseif(is_array($param_override)){
            $params = $param_override;

        } else {
            if($config['debug'] === TRUE){
                throw new StorageException(sprintf("Invalid Storage::exec call for class %s, action %s.", $class, $action));
            } else {
                flash('Oops, something went wrong.');
            }
        }

        foreach($params as $k => $v){
            $type = is_int($v) ? PDO::PARAM_INT : PDO::PARAM_STR; // Otherwise, everything is PDO::PARAM_STR, yay weak dynamic typing!
            $statement->bindValue($k, $v, $type);
        }

        $success = $statement->execute();
        $ret = FALSE;

        if($success){

            if($obj !== FALSE){

                if($action === 'add'){
                    // Add id property to added object.
                    $obj->id = $this->connection->lastInsertId();
                }
                $ret = $success;

            } elseif($action === 'list'){

                $data = $statement->fetchAll(PDO::FETCH_ASSOC);
                $ret = $data;

            } else {

                // This only gets executed on object load.
                $ret = $statement->fetch(PDO::FETCH_ASSOC);
            }

        } elseif($config['debug'] === TRUE){
            flash('Query failed');
            flash($statement->errorInfo());
        }
        
        $statement->closeCursor();
        return $ret;
    }

}



class BaseField extends BaseRenderable implements Field {

    public $type = 'VARCHAR(100)';
    public $empty = '';
    public $not_null = FALSE;
    public $name;
    protected $data;


    public function __construct($name, $value=NULL){
        $this->name = $name;

        if($value !== NULL){
            $this->value($value);
        } else {
            $this->value($this->empty);
        }
    }

    public function __toString(){
        return (string)$this->data;
    }

    
    public function value($value=NULL){
        if($value !== NULL){
            $this->data = $value;
        }

        return $this->data;
    }


    public function fill($data, $obj){
        $obj->$this->name = $data[$this->name];
    }

    public function save(){

        /** 
         * Saving any data external to the content objects own db table.
         *
         */
    }
}


class TextField extends BaseField {}



class IntField extends BaseField {}



class BaseStorable extends BaseHandler implements Storable {

    protected $fields = array(
        'test' => 'BaseField',
    );

    protected $field_instances;


    public function __construct(){

        $this->fields['id'] = 'IntField';
        $this->fields['name'] = 'TextField';

        $this->prepare_field_instances();
        $this->name = FALSE;

        foreach($this->fields as $k => $cls){
            $reflection = new ReflectionClass($cls);
            $instance = $reflection->newInstance($k);
            //$this->$k = $instance->empty; // Use field classes defined empty value
        }
    }


    public function __get($name){

        if(array_key_exists($name, $this->field_instances)){
            return $this->field_instances[$name]->value();
        }

        throw new StorageException(sprintf("Tried to get inexistent field '%s' from Storable '%s'.", $name, get_class($this)));
    }


    public function __set($name, $value){

        if(array_key_exists($name, $this->field_instances)){
            $this->field_instances[$name]->value($value);
        } else {

            throw new StorageException(sprintf("Tried to set inexistent field '%s' from Storable '%s'.", $name, get_class($this)));
        }
    }


    public function __isset($name){

        return array_key_exists($name, $this->field_instances);
    }


    public function __unset($name){

        if(array_key_exists($name, $this->field_instances)){
            $this->field_instances[$name]->value($this->field_instances[$name]->empty); // Empty field value
        }
    }


    public static function load($id_or_name){
        
        global $storage;

        $class = get_called_class();

        if(ctype_digit((string)$id_or_name)){
            $action = 'load';
        } else {
            $action = 'load_by_name';
        }

        $reflection = new ReflectionClass($class);
        $obj = $reflection->newInstance();


        $data = $storage->exec($class, $action, $id_or_name);
        if($data !== FALSE && count($data)){
            foreach($data as $k => $v){
                $obj->$k = $v; // TODO: How to keep $this->field_instances synchronized?
            }

            return $obj;

        } else {
            throw new StorageException(sprintf("Could not load %s %s", $class, $id_or_name));
        }
    }


    public static function listing($offset=0, $limit=NULL, $reverse=FALSE){

        /**
         * List stored instances of this type.
         *
         */

        global $config;
        global $storage;
        
        $class = get_called_class();
        $action = $reverse ? 'list_desc' : 'list';

        if(!is_int($offset) && ctype_digit($offset)){
            $offset = (int)$offset;
        }

        if(!is_int($limit)){
            if(ctype_digit($limit)){
                $limit = (int)$limit;
            } else {
                $limit = $config['pagination'];
            }
        }

        $rows = $storage->exec($class, $action,
            array(
                ':offset' => $offset,
                ':limit' => $limit,
            )
        );

        $reflection = new ReflectionClass($class);
        $load = $reflection->getMethod('load');

        $items = array();
        foreach($rows as $row){
            $items[] = $load->invokeArgs(NULL, $row);
        }

        return $items;

    }


    public function handle($mode='full'){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){

            var_dump('POST');
            var_dump($this);
            foreach(array_keys($this->get_field_instances()) as $name){
                try {
                    $this->$name = $_POST[$name];
                } catch(ValidationError $e){
                    flash(sprintf("Validation error on field '%s': %s", $name, $e->getMessage()));
                    redirect($_GET['p']);
                    return '';
                }
            }

            var_dump('updated object: "');
            var_dump($this);

            if($mode === 'add'){
            } elseif($mode === 'edit'){
            }
        } else {
            return parent::handle($mode);
        }
    }


    public function get_field_instances(){
        /* This function just exists for the query preparation in class Storage.
         * It has a bit of an unsatisfactory feel to it. Possible to solve this
         * more elegantly?
         */
        return $this->field_instances;
    }


    protected function prepare_field_instances(){

        $this->field_instances = array();

        foreach($this->fields as $name => $class){
            $this->field_instances[$name] = new $class($name);
        }
    }


    public function form(){

        $tpls = array(
            sprintf('form-%s.tpl.php', strtolower(get_class($this))),
            'form',
        );

        return render_template($tpls, array('obj' => $this));
    }


    public function save(){

        global $storage;

        $action = isset($this->id) ? 'update' : 'add';

        /*foreach(array_keys($this->fields) as $name){

            $field_instance = $this->field_instances[$name];


            $field_instance->value($this->$name);
            $field_instance->save();
        }*/

        $storage->exec($this, $action);

    }
}



class Listing extends BaseHandler {

    public $class;
    public $mode;
    public $items;

    public function __construct($class, $mode, $offset=0, $pagination=NULL){

        $this->class = $class;
        $this->mode = $mode;

        $reflection = new ReflectionClass($class);
        $method = $reflection->getMethod('listing');

        $data = $method->invokeArgs(NULL, array($offset, $pagination));

        $this->items = $data;

    }
}
