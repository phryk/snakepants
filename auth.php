<?php

/**
 * This file defines functionality related to authentication and authorization.
 *
 * i.e. user-related stuff
 */

require_once('storage.php');


class Registration extends BaseHandler {

    public function handle(){

        global $config;

        if($config['allow_registration'] === TRUE){

            if($_SERVER['REQUEST_METHOD'] == 'POST'){

                try {

                    $user = User::register($_POST['name'], $_POST['password']);

                } catch (Exception $e){

                    if($config['debug'] === TRUE){
                        $msg = $e->getMessage();
                    } else {
                        $msg = "An error occured while trying to register this user.";
                    }

                    $ep = ErrorPage($msg);
                    return $ep->handle();
                }


                $_SESSION['uid'] = $user->id;
                flash(sprintf("Welcome, %s. Registration was successful.", $user->name));

                redirect('admin');

            } else {

                return parent::handle();
            }

        } else {

            $ep = new ErrorPage("Registration not allowed.", 403);
            return $ep->handle();
        }
    }
}


class Login extends BaseHandler {

    public function handle(){
        if($_SERVER['REQUEST_METHOD'] == 'POST'){
            $user = User::login($_POST['name'], $_POST['password']);
            if($user !== FALSE){
                $_SESSION['uid'] = $user->id;
                flash(sprintf("Welcome, %s!", $user->name));
                redirect('');
            } else {
                flash("Name and/or password incorrect, try again.");
                redirect($_GET['p']);
            }

        } else {
            return parent::handle();
        }
    }

}



class Logout extends BaseHandler {

    public function handle(){
        unset($_SESSION['uid']);
        flash('You have been logged out.');
        redirect();
    }
}



class PasswordField extends BaseField {
    public $type = 'VARCHAR(128)';
}



class User extends BaseStorable {

    protected $fields = array(
        'password' => 'PasswordField',
    );

    
    public static function login($name, $pw){

        global $storage;

        // Generate a fallback hash for password_verify in any case so execution time stays similar.
        $fallback_hash = password_hash(openssl_random_pseudo_bytes(rand(8,15)), PASSWORD_BCRYPT);

        try {
            $user = new User($name);
        } catch(StorageException $e){
            password_verify('arbitrary', $fallback_hash);
            return FALSE;
        }

        if(password_verify($pw, $user->password)){
            return $user;
        }

        return FALSE;
    }


    public static function register($name, $pw){

        global $storage;
        $user = new User();

        $user->name = $name;
        $user->password = password_hash($pw, PASSWORD_BCRYPT);
        flash('Calling User::save');
        $user->save();

        return $user;
    }
}



/*class AuthHandler implements Handler {

    public function handle(){}
}*/
