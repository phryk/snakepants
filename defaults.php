<?php

$config = array(
    'debug' => FALSE,
    'theme' => 'default',
    'pagination' => 15,
    'base_url' => 'localhost/?p=', // Used for URL building, because $_SERVER['HTTP_HOST'] is set by the client and might lead to malicious redirects.
    'force_ssl_user' => FALSE, // Force SSL for logged in users.
    'force_ssl_anon' => FALSE, // Force SSL for anonymous users.
    'allow_registration' => TRUE,
);
